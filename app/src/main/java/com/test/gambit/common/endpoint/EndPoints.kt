package com.test.gambit.common.endpoint

import com.test.gambit.ui.games.models.GameResponse
import com.test.gambit.ui.players.models.PlayerResponseModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by stllpt031 on 27/4/19.
 */
interface EndPoints {

    @GET("api/v1/games")
    fun fetchGameList(
        @Query("page") pageIndex: Int = 0,
        @Query("per_page") perPage: Int = 10
    ):
            Observable<Response<GameResponse>>

    @GET("api/v1/players")
    fun fetchPlayersList(
        @Query("page") pageIndex: Int = 0,
        @Query("per_page") perPage: Int = 10
    ):
            Observable<Response<PlayerResponseModel>>


    @GET("api/v1/players")
    fun searchPlayer(
        @Query("page") pageIndex: Int = 0,
        @Query("per_page") perPage: Int = 20,
        @Query("search") search: String
    ):
            Observable<Response<PlayerResponseModel>>

}