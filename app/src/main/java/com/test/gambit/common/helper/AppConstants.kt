package com.test.gambit.common.helper

object AppConstants {

    // Database name
    const val DB_SPORTS = "dbSports"

    // Table name
    const val TABLE_PLAYERS = "PlayersMaster"
    const val TABLE_TEAM = "TeamsMaster"
    const val TABLE_GAMES = "GamesMaster"

    // Player Column names
    const val COLUMN_PLAYER_ID = "PlayerId"
    const val COLUMN_PLAYER_FIRST_NAME = "PlayerFirstName"
    const val COLUMN_PLAYER_LAST_NAME = "PlayerLastName"
    const val COLUMN_PLAYER_POSITION = "PlayerPosition"
    const val COLUMN_PLAYER_TEAM_ID = "PlayerTeamId"

    // Team column names
    const val COLUMN_TEAM_ID = "TeamId"
    const val COLUMN_TEAM_ABBREVIATION = "TeamAbbreviation"
    const val COLUMN_TEAM_CONFERENCE = "TeamConference"
    const val COLUMN_TEAM_DIVISION = "TeamDivision"
    const val COLUMN_TEAM_FULL_NAME = "TeamFullName"

    // Game column names
    const val COLUMN_GAME_ID = "GameId"
    const val COLUMN_HOME_TEAM_ID = "HomeTeamId"
    const val COLUMN_VISITOR_TEAM_ID = "VisitorTeamId"
    const val COLUMN_HOME_TEAM_NAME = "HomeTeamName"
    const val COLUMN_VISITOR_TEAM_NAME = "VisitorTeamName"
    const val COLUMN_HOME_TEAM_FULL_NAME = "HomeTeamFullName"
    const val COLUMN_VISITOR_TEAM_FULL_NAME = "VisitorTeamFullName"

    const val BASE_URL = "https://www.balldontlie.io/"
}