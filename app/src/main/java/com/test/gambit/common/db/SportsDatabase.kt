package com.test.gambit.common.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.gambit.ui.games.db.GamesDao
import com.test.gambit.ui.games.db.GamesEntity
import com.test.gambit.ui.players.db.PlayerDao
import com.test.gambit.ui.players.db.PlayerEntity
import com.test.gambit.ui.players.db.TeamEntity


@Database(
    entities = [PlayerEntity::class, TeamEntity::class, GamesEntity::class],
    version = 1, exportSchema = false
)
public abstract class SportsDatabase : RoomDatabase() {
    abstract fun PlayerDao(): PlayerDao
    abstract fun GamesDao(): GamesDao
}