package com.test.gambit.injection.module

import com.test.gambit.AppApplication
import com.test.gambit.common.db.SportsDatabase
import com.test.gambit.common.helper.AppConstants
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomDbModule {
    @Provides
    @Singleton
    fun provideRoomDatabase(): SportsDatabase =
        Room.databaseBuilder(
            AppApplication.instance,
            SportsDatabase::class.java,
            AppConstants.DB_SPORTS
        )
            .fallbackToDestructiveMigration()
            .build()
}