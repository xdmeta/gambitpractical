package com.test.gambit.injection.components

import com.test.gambit.injection.module.RetrofitModule
import com.test.gambit.injection.module.RoomDbModule
import com.test.gambit.injection.module.ViewModelFactory
import com.test.gambit.injection.module.ViewModelModule
import com.test.gambit.ui.games.view.GamesFragment
import com.test.gambit.ui.players.view.PlayersFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [RoomDbModule::class,
        RetrofitModule::class,
        ViewModelModule::class]
)
interface AppComponents {
    fun inject(fragment: GamesFragment)
    fun inject(fragment: PlayersFragment)
}