package com.test.gambit.ui.home

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.test.gambit.ui.games.view.GamesFragment
import com.test.gambit.ui.players.view.PlayersFragment

/**
 * Created by stllpt031 on 28/4/19.
 */
class HomeAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int) = when (position) {
        0 -> PlayersFragment()
        else -> GamesFragment()
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int) = when (position) {
        0 -> "Players"
        else -> "Games"
    }
}