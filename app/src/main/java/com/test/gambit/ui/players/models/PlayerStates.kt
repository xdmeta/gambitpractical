package com.test.gambit.ui.players.models

/**
 * Created by stllpt031 on 27/4/19.
 */


data class Error(val msg: String = "", val show: Boolean = false)

data class Response(val response: List<DataItem> ?= null)

data class Progress(val show: Boolean = false)

data class PlayerStates(
    val error: Error = Error(),
    val response: Response = Response(),
    val progress: Progress = Progress()
)