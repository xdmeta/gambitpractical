package com.test.gambit.ui.games.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.test.gambit.common.helper.AppConstants

/**
 * Created by stllpt031 on 27/4/19.
 */
@Entity(tableName = AppConstants.TABLE_GAMES)
data class GamesEntity (
    @PrimaryKey()
    @ColumnInfo(name = AppConstants.COLUMN_GAME_ID)
    var gameId : Int = 0,

    @ColumnInfo(name = AppConstants.COLUMN_HOME_TEAM_ID)
    var homeTeamId : Int = 0,

    @ColumnInfo(name = AppConstants.COLUMN_VISITOR_TEAM_ID)
    var visitorTeamId : Int = 0,

    @ColumnInfo(name = AppConstants.COLUMN_HOME_TEAM_NAME)
    var homeTeamName : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_VISITOR_TEAM_NAME)
    var visitorTeamName : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_HOME_TEAM_FULL_NAME)
    var homeTeamFullName : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_VISITOR_TEAM_FULL_NAME)
    var visitorTeamFullName : String = ""

)