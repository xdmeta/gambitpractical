package com.test.gambit.ui.players.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.gambit.R
import com.test.gambit.ui.players.models.DataItem
import kotlinx.android.synthetic.main.layout_players_item.view.*

/**
 * Created by stllpt031 on 28/4/19.
 */
class PlayersAdapter(private val itemList: ArrayList<DataItem>) : RecyclerView.Adapter<PlayersAdapter.PlayerHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerHolder {
        return PlayerHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_players_item, parent, false))
    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: PlayerHolder, position: Int) {
        holder.itemView.apply {
            itemList[position].let {
                tvFirstName.text = it.firstName
                tvLastName.text = it.lastName
                tvTeamName.text = it.team?.abbreviation
                tvTeamId.text = it.team?.id?.toString()
                tvPosition.text = it.position
                tvFullName.text = it.team?.fullName
                tvConference.text = it.team?.abbreviation
                tvDivision.text = it.team?.division
            }
        }
    }

    inner class PlayerHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}