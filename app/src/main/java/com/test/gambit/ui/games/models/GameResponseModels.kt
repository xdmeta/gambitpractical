package com.test.gambit.ui.games.models


import com.google.gson.annotations.SerializedName

data class Meta(@SerializedName("next_page")
                val nextPage: Int? = 0,
                @SerializedName("per_page")
                val perPage: Int? = 0,
                @SerializedName("total_count")
                val totalCount: Int? = 0,
                @SerializedName("total_pages")
                val totalPages: Int? = 0,
                @SerializedName("current_page")
                val currentPage: Int? = 0)


data class HomeTeam(@SerializedName("division")
                    val division: String? = "",
                    @SerializedName("conference")
                    val conference: String? = "",
                    @SerializedName("full_name")
                    val fullName: String? = "",
                    @SerializedName("city")
                    val city: String? = "",
                    @SerializedName("name")
                    val name: String? = "",
                    @SerializedName("id")
                    val id: Int? = 0,
                    @SerializedName("abbreviation")
                    val abbreviation: String? = "")


data class DataItem(@SerializedName("date")
                    val date: String? = "",
                    @SerializedName("postseason")
                    val postseason: Boolean? = false,
                    @SerializedName("period")
                    val period: Int? = 0,
                    @SerializedName("season")
                    val season: Int? = 0,
                    @SerializedName("visitor_team_score")
                    val visitorTeamScore: Int? = 0,
                    @SerializedName("visitor_team")
                    val visitorTeam: VisitorTeam? = VisitorTeam(),
                    @SerializedName("id")
                    val id: Int? = 0,
                    @SerializedName("home_team_score")
                    val homeTeamScore: Int? = 0,
                    @SerializedName("time")
                    val time: String? = "",
                    @SerializedName("home_team")
                    val homeTeam: HomeTeam? = HomeTeam(),
                    @SerializedName("status")
                    val status: String? = "")


data class GameResponse(@SerializedName("data")
                             val data: List<DataItem>? = null,
                        @SerializedName("meta")
                             val meta: Meta? = null)


data class VisitorTeam(@SerializedName("division")
                       val division: String? = "",
                       @SerializedName("conference")
                       val conference: String? = "",
                       @SerializedName("full_name")
                       val fullName: String? = "",
                       @SerializedName("city")
                       val city: String? = "",
                       @SerializedName("name")
                       val name: String? = "",
                       @SerializedName("id")
                       val id: Int? = 0,
                       @SerializedName("abbreviation")
                       val abbreviation: String? = "")


