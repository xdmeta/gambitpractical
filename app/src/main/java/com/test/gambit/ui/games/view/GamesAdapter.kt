package com.test.gambit.ui.games.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.gambit.R
import com.test.gambit.ui.games.models.DataItem
import com.test.gambit.ui.games.models.GameResponse
import kotlinx.android.synthetic.main.layout_games_item.view.*

/**
 * Created by stllpt031 on 28/4/19.
 */
class GamesAdapter(private val itemList: ArrayList<DataItem>) :
    RecyclerView.Adapter<GamesAdapter.GamesHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        GamesHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_games_item,
                parent, false
            )
        )

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: GamesHolder, position: Int) {
        holder.itemView.apply {
            itemList[position].let {
                tvTeamName.text = it.homeTeam?.abbreviation
                tvTeamFullName.text = it.homeTeam?.fullName
                tvOpponentTeamName.text = it.visitorTeam?.abbreviation
                tvOpponentTeamFullName.text = it.visitorTeam?.fullName
            }
        }
    }

    inner class GamesHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}