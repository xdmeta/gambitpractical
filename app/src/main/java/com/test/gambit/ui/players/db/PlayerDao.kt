package com.test.gambit.ui.players.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.gambit.common.helper.AppConstants
import io.reactivex.Single

@Dao
public interface PlayerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPlayersData(playerEntities: Array<PlayerEntity>)

    @Query("Delete from ${AppConstants.TABLE_PLAYERS}")
    fun nukeTable()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTeamData(teamEntity: Array<TeamEntity>)

    @Query("SELECT ${AppConstants.TABLE_PLAYERS}.*, ${AppConstants.TABLE_TEAM}.* FROM ${AppConstants.TABLE_PLAYERS} INNER JOIN ${AppConstants.TABLE_TEAM} ON ${AppConstants.TABLE_PLAYERS}.${AppConstants.COLUMN_PLAYER_TEAM_ID} = ${AppConstants.TABLE_TEAM}.${AppConstants.COLUMN_TEAM_ID}")
    fun fetchAllPlayers(): Single<List<PlayerTeamEntity>>

}