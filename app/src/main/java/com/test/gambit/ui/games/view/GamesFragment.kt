package com.test.gambit.ui.games.view

import android.content.ContentValues
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.test.gambit.AppApplication
import com.test.gambit.R
import com.test.gambit.common.base.BaseFragment
import com.test.gambit.common.extensions.isVisible
import com.test.gambit.common.helper.CustomLayoutManager
import com.test.gambit.ui.games.models.DataItem
import com.test.gambit.ui.games.viewmodel.GamesViewModel
import kotlinx.android.synthetic.main.layout_sports_item_list.*
import javax.inject.Inject

/**
 * Created by stllpt031 on 28/4/19.
 */
class GamesFragment : BaseFragment() {

    private val itemList = ArrayList<DataItem>()
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val addItemViewModel: GamesViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[GamesViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.layout_sports_item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity?.application as AppApplication).mComponent.inject(this)
        initItemList()
        initViewObserver()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            try {
                val mImm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                mImm.hideSoftInputFromWindow(view!!.windowToken, 0)
                mImm.hideSoftInputFromWindow(activity!!.currentFocus!!.windowToken, 0)
            } catch (e: Exception) {
                Log.e(ContentValues.TAG, "setUserVisibleHint: ", e)
            }
        }
    }

    private fun initViewObserver() {
        addItemViewModel.fetchGamesList(compositeDisposable)
        addItemViewModel.gamesModel.observe(this, Observer {
            progress.visibility = it.progress.show.isVisible()
            rvSports.visibility = (it.response.gameResponse != null).isVisible()
            tvError.visibility = it.error.show.isVisible()

            it.response.gameResponse?.let {
                it.let { it1 -> itemList.addAll(it1) }
                rvSports.adapter?.notifyItemRangeChanged(0, itemList.size)
            }
            if (it.error.show) {
                tvError.text = it.error.msg
            }
        })
    }

    private fun initItemList() {
        rvSports.layoutManager = context?.let { CustomLayoutManager(it) }
        rvSports.adapter = GamesAdapter(itemList)
    }
}