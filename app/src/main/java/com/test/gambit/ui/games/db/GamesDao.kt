package com.test.gambit.ui.games.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.gambit.common.helper.AppConstants
import com.test.gambit.ui.players.db.TeamEntity
import io.reactivex.Single

/**
 * Created by stllpt031 on 27/4/19.
 */
@Dao
public interface GamesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGamesData(gamesEntities: Array<GamesEntity>)

    @Query("Delete from ${AppConstants.TABLE_GAMES}")
    fun nukeTable()

    @Query("Select * From ${AppConstants.TABLE_GAMES}")
    fun fetchAllGames(): Single<List<GamesEntity>>
}