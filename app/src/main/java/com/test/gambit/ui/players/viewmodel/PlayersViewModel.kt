package com.test.gambit.ui.players.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.gambit.AppApplication
import com.test.gambit.common.db.SportsDatabase
import com.test.gambit.common.endpoint.EndPoints
import com.test.gambit.common.extensions.isInternetAvailable
import com.test.gambit.ui.players.db.PlayerEntity
import com.test.gambit.ui.players.db.TeamEntity
import com.test.gambit.ui.players.models.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by stllpt031 on 27/4/19.
 */
class PlayersViewModel @Inject constructor(
    private val api: EndPoints,
    private val sportsDb: SportsDatabase
) : ViewModel() {
    val playerModel = MutableLiveData<PlayerStates>()
    var pageIndex = 0

    fun fetchPlayersList(compositeDisposable: CompositeDisposable, searchQuery: String? = null) {
        if (!AppApplication.instance.isInternetAvailable()) {
            Observable.just(1)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribeBy(
                    onNext = {
                        fetchOfflinePlayerList(compositeDisposable)
                    }, onError = {
                        it.printStackTrace()
                    }
                ).addTo(compositeDisposable)
        } else {
            val api = searchQuery?.let {
                api.searchPlayer(pageIndex, search = it)
            } ?: let {
                api.fetchPlayersList(pageIndex)
            }
            api.observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { playerModel.value = PlayerStates(progress = Progress(true)) }
                .subscribeBy(onNext = {
                    when {
                        it.isSuccessful -> {
                            val playerResponseModel = it.body()
                            playerModel.value = PlayerStates(response = Response(playerResponseModel?.data))
                            searchQuery ?: let { _ ->
                                Observable.just(1)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(Schedulers.io())
                                    .subscribeBy(
                                        onNext = {
                                            sportsDb.PlayerDao().nukeTable()
                                            playerResponseModel?.data?.let {
                                                storePlayersOffline(it)
                                            }
                                        }, onError = {
                                            it.printStackTrace()
                                        }
                                    )
                            }
                        }
                        else -> {
                            playerModel.value = PlayerStates(error = Error(it.message(), true))
                        }
                    }
                }, onError = {
                    playerModel.value = PlayerStates(
                        error = Error(
                            it.message ?: "Unknown error",
                            true
                        )
                    )
                }).addTo(compositeDisposable)
        }
    }

    private fun storePlayersOffline(
        playersList: List<DataItem>
    ) {
        val playerData: List<PlayerEntity> = playersList
            .map {
                PlayerEntity(
                    playerId = it.id ?: 0,
                    playerFirstName = it.firstName ?: "",
                    playerLastName = it.lastName ?: "",
                    playerPosition = it.position ?: "",
                    playerTeamId = it.team?.id ?: 0
                )
            }
        sportsDb.PlayerDao().insertPlayersData(playerData.toTypedArray())

        val teamData: List<TeamEntity> = playersList
            .map {
                TeamEntity(
                    teamId = it.team?.id ?: 0,
                    teamAbbreviation = it.team?.abbreviation ?: "",
                    teamConference = it.team?.conference ?: "",
                    teamDivision = it.team?.division ?: "",
                    teamFullName = it.team?.fullName ?: ""
                )
            }.distinct()
        sportsDb.PlayerDao().insertTeamData(teamData.toTypedArray())
    }

    private fun fetchOfflinePlayerList(compositeDisposable: CompositeDisposable) {
        sportsDb.PlayerDao().fetchAllPlayers().map { it ->
            it.map {
                DataItem(
                    firstName = it.playerEntity.playerFirstName,
                    lastName = it.playerEntity.playerLastName,
                    position = it.playerEntity.playerPosition,
                    id = it.playerEntity.playerId,
                    team = Team(
                        id = it.teamEntity.teamId,
                        division = it.teamEntity.teamDivision,
                        conference = it.teamEntity.teamConference,
                        fullName = it.teamEntity.teamFullName,
                        city = "",
                        name = it.teamEntity.teamFullName,
                        abbreviation = it.teamEntity.teamAbbreviation
                    )
                )
            }
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    playerModel.value = PlayerStates(response = Response(it))
                }, onError = {
                    playerModel.value = PlayerStates(
                        error = Error(it.message ?: "Unknown error", true)
                    )
                }
            ).addTo(compositeDisposable)
    }


}