package com.test.gambit.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import com.test.gambit.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var scrollRange = -1
    private var isShow = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupActionBar()
        setupTabs()
    }

    private fun setupTabs() {
        tabs.setupWithViewPager(vpContent)
        vpContent.adapter = HomeAdapter(supportFragmentManager)
    }

    private fun setupActionBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.app_title)
        val appBarLayout: AppBarLayout = findViewById(com.test.gambit.R.id.appBar)
        appBarLayout.addOnOffsetChangedListener(
            AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
                appBarLayout.post {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.totalScrollRange
                    }
                    if (scrollRange + verticalOffset == 0) {
                        supportActionBar?.title = getString(R.string.app_title)
                        isShow = true
                    } else if (isShow) {
                        supportActionBar?.title =
                                " "//careful there should a space between double quote otherwise it wont work
                        isShow = false
                    }
                }

            })
    }
}
