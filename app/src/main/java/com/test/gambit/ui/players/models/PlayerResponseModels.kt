package com.test.gambit.ui.players.models


import com.google.gson.annotations.SerializedName

data class Meta(@SerializedName("next_page")
                val nextPage: Int? = 0,
                @SerializedName("per_page")
                val perPage: Int? = 0,
                @SerializedName("total_count")
                val totalCount: Int? = 0,
                @SerializedName("total_pages")
                val totalPages: Int? = 0,
                @SerializedName("current_page")
                val currentPage: Int? = 0)


data class DataItem(@SerializedName("last_name")
                    val lastName: String? = "",
                    @SerializedName("id")
                    val id: Int? = 0,
                    @SerializedName("position")
                    val position: String? = "",
                    @SerializedName("team")
                    val team: Team?,
                    @SerializedName("first_name")
                    val firstName: String? = "")


data class PlayerResponseModel(@SerializedName("data")
                               val data: List<DataItem>? = null,
                               @SerializedName("meta")
                               val meta: Meta?)


data class Team(@SerializedName("division")
                val division: String? = "",
                @SerializedName("conference")
                val conference: String? = "",
                @SerializedName("full_name")
                val fullName: String? = "",
                @SerializedName("city")
                val city: String? = "",
                @SerializedName("name")
                val name: String? = "",
                @SerializedName("id")
                val id: Int? = 0,
                @SerializedName("abbreviation")
                val abbreviation: String? = "")


