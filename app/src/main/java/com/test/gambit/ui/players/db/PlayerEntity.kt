package com.test.gambit.ui.players.db

import com.test.gambit.common.helper.AppConstants
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = AppConstants.TABLE_PLAYERS)
data class PlayerEntity(
    @PrimaryKey()
    @ColumnInfo(name = AppConstants.COLUMN_PLAYER_ID)
    var playerId : Int = 0,

    @ColumnInfo(name = AppConstants.COLUMN_PLAYER_FIRST_NAME)
    var playerFirstName : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_PLAYER_LAST_NAME)
    var playerLastName : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_PLAYER_POSITION)
    var playerPosition : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_PLAYER_TEAM_ID)
    var playerTeamId : Int = 0
)