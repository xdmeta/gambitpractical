package com.test.gambit.ui.games.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.gambit.AppApplication
import com.test.gambit.common.db.SportsDatabase
import com.test.gambit.common.endpoint.EndPoints
import com.test.gambit.common.extensions.isInternetAvailable
import com.test.gambit.ui.games.db.GamesEntity
import com.test.gambit.ui.games.models.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by stllpt031 on 27/4/19.
 */
class GamesViewModel @Inject constructor(
    private val api: EndPoints,
    private val sportsDb: SportsDatabase
) :
    ViewModel() {
    val gamesModel = MutableLiveData<GameStates>()
    private var pageIndex = 0
    fun fetchGamesList(compositeDisposable: CompositeDisposable) {
        if (!AppApplication.instance.isInternetAvailable()) {
            Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .subscribeBy(
                    onNext = {
                        fetchOfflineGamesList(compositeDisposable)
                    }, onError = {
                        it.printStackTrace()
                    }
                ).addTo(compositeDisposable)

        } else {
            api.fetchGameList(pageIndex)
                .doOnSubscribe {
                    gamesModel.value = GameStates(progress = Progress(true))
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = {
                    when {
                        it.isSuccessful -> {
                            val gameResponseModel = it.body()?.data
                            gamesModel.value = GameStates(response = Response(gameResponseModel))
                            Observable.just(1)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(Schedulers.newThread())
                                .subscribeBy(
                                    onNext = {
                                        sportsDb.GamesDao().nukeTable()
                                        gameResponseModel?.let {
                                            storeGamesOffline(it)
                                        }
                                    }, onError = {
                                        it.printStackTrace()
                                    }
                                )
                        }
                        else -> {
                            gamesModel.value = GameStates(error = Error(it.message(), true))
                        }
                    }
                }, onError = {
                    gamesModel.value = GameStates(
                        error = Error(
                            it.message ?: "Unknown error",
                            true
                        )
                    )
                }).addTo(compositeDisposable)

        }
    }

    private fun storeGamesOffline(gamesList: List<DataItem>) {
        val gamesData: List<GamesEntity> = gamesList
            .map {
                GamesEntity(
                    gameId = it.id ?: 0,
                    homeTeamId = it.homeTeam?.id ?: 0,
                    homeTeamName = it.homeTeam?.abbreviation ?: "",
                    homeTeamFullName = it.homeTeam?.fullName ?: "",
                    visitorTeamId = it.visitorTeam?.id ?: 0,
                    visitorTeamName = it.visitorTeam?.abbreviation ?: "",
                    visitorTeamFullName = it.visitorTeam?.fullName ?: ""
                )
            }
        sportsDb.GamesDao().insertGamesData(gamesData.toTypedArray())
    }

    private fun fetchOfflineGamesList(compositeDisposable: CompositeDisposable) {
        sportsDb.GamesDao().fetchAllGames().map { it ->
            it.map {
                DataItem().copy(
                    id = it.gameId,
                    homeTeam = HomeTeam().copy(
                        id = it.homeTeamId,
                        abbreviation = it.homeTeamName,
                        fullName = it.homeTeamFullName
                    ),
                    visitorTeam = VisitorTeam().copy(
                        id = it.visitorTeamId,
                        abbreviation = it.visitorTeamName,
                        fullName = it.homeTeamFullName
                    )
                )
            }
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    gamesModel.value = GameStates(response = Response(it))
                }, onError = {
                    gamesModel.value = GameStates(
                        error = Error(it.message ?: "Unknown error", true)
                    )
                }
            ).addTo(compositeDisposable)

    }
}