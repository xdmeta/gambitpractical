package com.test.gambit.ui.players.db

import androidx.room.Embedded

/**
 * Created by stllpt031 on 29/4/19.
 */
class PlayerTeamEntity {

    @Embedded
    lateinit var playerEntity: PlayerEntity

    @Embedded
    lateinit var teamEntity: TeamEntity
}