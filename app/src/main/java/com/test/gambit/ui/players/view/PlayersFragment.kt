package com.test.gambit.ui.players.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.jakewharton.rxbinding2.widget.textChanges
import com.test.gambit.AppApplication
import com.test.gambit.R
import com.test.gambit.common.base.BaseFragment
import com.test.gambit.common.extensions.isInternetAvailable
import com.test.gambit.common.extensions.isVisible
import com.test.gambit.common.extensions.visible
import com.test.gambit.common.helper.CustomLayoutManager
import com.test.gambit.ui.players.models.DataItem
import com.test.gambit.ui.players.viewmodel.PlayersViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.layout_sports_item_list.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by stllpt031 on 28/4/19.
 */
class PlayersFragment : BaseFragment() {

    private val itemList = ArrayList<DataItem>()
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val playersViewModel: PlayersViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[PlayersViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.layout_sports_item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity?.application as AppApplication).mComponent.inject(this)
        initItemList()
        fetchInitialData()
        setupSearchBar()
    }

    private fun setupSearchBar() {
        rlSearch.visible()
        etSearchPlayer.textChanges()
            .skip(1)
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { playerName ->
                    itemList.clear()
                    playersViewModel.fetchPlayersList(compositeDisposable, playerName.toString())
                    setupObserver()
                },
                onError = {
                    it.printStackTrace()
                }
            ).addTo(compositeDisposable)
    }

    private fun initItemList() {
        rvSports.layoutManager = context?.let { CustomLayoutManager(it) }
        rvSports.adapter = PlayersAdapter(itemList)
    }

    private fun fetchInitialData() {
        playersViewModel.fetchPlayersList(compositeDisposable)
        setupObserver()
    }

    private fun setupObserver() {
        playersViewModel.playerModel.observe(this, Observer {
            progress.visibility = it.progress.show.isVisible()
            tvError.visibility = it.error.show.isVisible()
            rvSports.visibility = (it.response.response != null).isVisible()
            it.response.response?.let {
                itemList.addAll(it)
                rvSports.adapter?.notifyItemRangeInserted(0, itemList.size)
            }
            if (it.progress.show) {
                itemList.clear()
                rvSports.adapter?.notifyItemRangeRemoved(0, itemList.size)
            }
        })

    }

}