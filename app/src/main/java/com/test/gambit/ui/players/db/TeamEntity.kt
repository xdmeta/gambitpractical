package com.test.gambit.ui.players.db

import com.test.gambit.common.helper.AppConstants
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = AppConstants.TABLE_TEAM)
data class TeamEntity(
    @PrimaryKey()
    @ColumnInfo(name = AppConstants.COLUMN_TEAM_ID)
    var teamId : Int = 0,

    @ColumnInfo(name = AppConstants.COLUMN_TEAM_ABBREVIATION)
    var teamAbbreviation : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_TEAM_CONFERENCE)
    var teamConference : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_TEAM_DIVISION)
    var teamDivision : String = "",

    @ColumnInfo(name = AppConstants.COLUMN_TEAM_FULL_NAME)
    var teamFullName : String = ""
)