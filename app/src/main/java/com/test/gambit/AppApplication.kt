package com.test.gambit

import android.app.Application
import com.test.gambit.injection.components.AppComponents
import com.test.gambit.injection.components.DaggerAppComponents
import com.test.gambit.injection.module.RetrofitModule
import com.test.gambit.injection.module.RoomDbModule

/**
 * Created by stllpt031 on 27/4/19.
 */

class AppApplication : Application() {
    lateinit var mComponent: AppComponents
    companion object {
        lateinit var instance : AppApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initDagger()
    }

    private fun initDagger() {
        mComponent = DaggerAppComponents.builder()
            .roomDbModule(RoomDbModule())
            .retrofitModule(RetrofitModule())
            .build()
    }
}